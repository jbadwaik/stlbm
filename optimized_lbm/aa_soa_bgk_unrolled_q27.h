// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of AA-pattern structure-of-array, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_bgk_unrolled {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6) + f(i, 9) + f(i,10) + f(i,11) + f(i,12);
        double X_P1 = f(i,14) + f(i,17) + f(i,18) + f(i,19) + f(i,20) + f(i,23) + f(i,24) + f(i,25) + f(i,26);
        double X_0  = f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,13) + f(i,15) + f(i,16) + f(i,21) + f(i,22);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 9) + f(i,10) + f(i,18) + f(i,25) + f(i,26);
        double Y_P1 = f(i,15) + f(i,17) + f(i,21) + f(i,22) + f(i,23) + f(i,24) + f(i, 4) + f(i,11) + f(i,12);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 9) + f(i,11) + f(i,20) + f(i,22) + f(i,24) + f(i,26);
        double Z_P1 = f(i,16) + f(i,19) + f(i,21) + f(i,23) + f(i,25) + f(i, 6) + f(i, 8) + f(i,10) + f(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 27> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

};

struct Even : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideBgkUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];

        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // BGK Collision based on the second-order equilibrium
        std::array<double, 27> foutRM;

        foutRM[F000] = (1. - omega) * f(i,F000) + omega * feqRM[F000];

        foutRM[FP00] = (1. - omega) * f(i,FP00) + omega * feqRM[FP00];
        foutRM[FM00] = (1. - omega) * f(i,FM00) + omega * feqRM[FM00];

        foutRM[F0P0] = (1. - omega) * f(i,F0P0) + omega * feqRM[F0P0];
        foutRM[F0M0] = (1. - omega) * f(i,F0M0) + omega * feqRM[F0M0];

        foutRM[F00P] = (1. - omega) * f(i,F00P) + omega * feqRM[F00P];
        foutRM[F00M] = (1. - omega) * f(i,F00M) + omega * feqRM[F00M];

        foutRM[FPP0] = (1. - omega) * f(i,FPP0) + omega * feqRM[FPP0];
        foutRM[FMP0] = (1. - omega) * f(i,FMP0) + omega * feqRM[FMP0];
        foutRM[FPM0] = (1. - omega) * f(i,FPM0) + omega * feqRM[FPM0];
        foutRM[FMM0] = (1. - omega) * f(i,FMM0) + omega * feqRM[FMM0];

        foutRM[FP0P] = (1. - omega) * f(i,FP0P) + omega * feqRM[FP0P];
        foutRM[FM0P] = (1. - omega) * f(i,FM0P) + omega * feqRM[FM0P];
        foutRM[FP0M] = (1. - omega) * f(i,FP0M) + omega * feqRM[FP0M];
        foutRM[FM0M] = (1. - omega) * f(i,FM0M) + omega * feqRM[FM0M];

        foutRM[F0PP] = (1. - omega) * f(i,F0PP) + omega * feqRM[F0PP];
        foutRM[F0MP] = (1. - omega) * f(i,F0MP) + omega * feqRM[F0MP];
        foutRM[F0PM] = (1. - omega) * f(i,F0PM) + omega * feqRM[F0PM];
        foutRM[F0MM] = (1. - omega) * f(i,F0MM) + omega * feqRM[F0MM];

        foutRM[FPPP] = (1. - omega) * f(i,FPPP) + omega * feqRM[FPPP];
        foutRM[FMPP] = (1. - omega) * f(i,FMPP) + omega * feqRM[FMPP];
        foutRM[FPMP] = (1. - omega) * f(i,FPMP) + omega * feqRM[FPMP];
        foutRM[FPPM] = (1. - omega) * f(i,FPPM) + omega * feqRM[FPPM];
        foutRM[FMMP] = (1. - omega) * f(i,FMMP) + omega * feqRM[FMMP];
        foutRM[FMPM] = (1. - omega) * f(i,FMPM) + omega * feqRM[FMPM];
        foutRM[FPMM] = (1. - omega) * f(i,FPMM) + omega * feqRM[FPMM];
        foutRM[FMMM] = (1. - omega) * f(i,FMMM) + omega * feqRM[FMMM];

        f(i,F000) = foutRM[F000];

        f(i,FP00) = foutRM[FM00];
        f(i,FM00) = foutRM[FP00];

        f(i,F0P0) = foutRM[F0M0];
        f(i,F0M0) = foutRM[F0P0];

        f(i,F00P) = foutRM[F00M];
        f(i,F00M) = foutRM[F00P];

        f(i,FPP0) = foutRM[FMM0];
        f(i,FMP0) = foutRM[FPM0];
        f(i,FPM0) = foutRM[FMP0];
        f(i,FMM0) = foutRM[FPP0];

        f(i,FP0P) = foutRM[FM0M];
        f(i,FM0P) = foutRM[FP0M];
        f(i,FP0M) = foutRM[FM0P];
        f(i,FM0M) = foutRM[FP0P];

        f(i,F0PP) = foutRM[F0MM];
        f(i,F0MP) = foutRM[F0PM];
        f(i,F0PM) = foutRM[F0MP];
        f(i,F0MM) = foutRM[F0PP];

        f(i,FPPP) = foutRM[FMMM];
        f(i,FMPP) = foutRM[FPMM];
        f(i,FPMP) = foutRM[FMPM];
        f(i,FPPM) = foutRM[FMMP];
        f(i,FMMP) = foutRM[FPPM];
        f(i,FMPM) = foutRM[FPMP];
        f(i,FPMM) = foutRM[FMPP];
        f(i,FMMM) = foutRM[FPPP];
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideBgkUnrolled(i, rho, u, usqr);
        }
    }

    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

struct Odd : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideBgkUnrolled(std::array<double, 27>& pop, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];

        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // BGK Collision based on the second-order equilibrium
        pop[F000] = (1. - omega) * pop[F000] + omega * feqRM[F000];

        pop[FP00] = (1. - omega) * pop[FP00] + omega * feqRM[FP00];
        pop[FM00] = (1. - omega) * pop[FM00] + omega * feqRM[FM00];

        pop[F0P0] = (1. - omega) * pop[F0P0] + omega * feqRM[F0P0];
        pop[F0M0] = (1. - omega) * pop[F0M0] + omega * feqRM[F0M0];

        pop[F00P] = (1. - omega) * pop[F00P] + omega * feqRM[F00P];
        pop[F00M] = (1. - omega) * pop[F00M] + omega * feqRM[F00M];

        pop[FPP0] = (1. - omega) * pop[FPP0] + omega * feqRM[FPP0];
        pop[FMP0] = (1. - omega) * pop[FMP0] + omega * feqRM[FMP0];
        pop[FPM0] = (1. - omega) * pop[FPM0] + omega * feqRM[FPM0];
        pop[FMM0] = (1. - omega) * pop[FMM0] + omega * feqRM[FMM0];

        pop[FP0P] = (1. - omega) * pop[FP0P] + omega * feqRM[FP0P];
        pop[FM0P] = (1. - omega) * pop[FM0P] + omega * feqRM[FM0P];
        pop[FP0M] = (1. - omega) * pop[FP0M] + omega * feqRM[FP0M];
        pop[FM0M] = (1. - omega) * pop[FM0M] + omega * feqRM[FM0M];

        pop[F0PP] = (1. - omega) * pop[F0PP] + omega * feqRM[F0PP];
        pop[F0MP] = (1. - omega) * pop[F0MP] + omega * feqRM[F0MP];
        pop[F0PM] = (1. - omega) * pop[F0PM] + omega * feqRM[F0PM];
        pop[F0MM] = (1. - omega) * pop[F0MM] + omega * feqRM[F0MM];

        pop[FPPP] = (1. - omega) * pop[FPPP] + omega * feqRM[FPPP];
        pop[FMPP] = (1. - omega) * pop[FMPP] + omega * feqRM[FMPP];
        pop[FPMP] = (1. - omega) * pop[FPMP] + omega * feqRM[FPMP];
        pop[FPPM] = (1. - omega) * pop[FPPM] + omega * feqRM[FPPM];
        pop[FMMP] = (1. - omega) * pop[FMMP] + omega * feqRM[FMMP];
        pop[FMPM] = (1. - omega) * pop[FMPM] + omega * feqRM[FMPM];
        pop[FPMM] = (1. - omega) * pop[FPMM] + omega * feqRM[FPMM];
        pop[FMMM] = (1. - omega) * pop[FMMM] + omega * feqRM[FMMM];
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 27> pop;
            if (periodic) {
                for (int k = 0; k < 27; ++k) {
                    int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = f(i, k) + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    int XX = iX - c[k][0];
                    int YY = iY - c[k][1];
                    int ZZ = iZ - c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = f(i, k) + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }

            auto[rho, u] = macropop(pop);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideBgkUnrolled(pop, rho, u, usqr);

            if (periodic) {
                for (int k = 0; k < 27; ++k) {
                    int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        f(i, opp[k]) = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    int XX = iX + c[k][0];
                    int YY = iY + c[k][1];
                    int ZZ = iZ + c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        f(i, opp[k]) = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
        }
    }

    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

} // namespace aa_soa_bgk_unrolled
