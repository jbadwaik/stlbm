// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of double-population structure-of-array, for O2-BGK and TRT, without aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace twopop_soa {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    // The "f" operator always refers to the population with parity 0, in which
    // the momentum-exchange term is stored.
    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            fin(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i, 10) + fin(i, 13) + fin(i, 14) + fin(i, 15) + fin(i, 16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i, 11) + fin(i, 12) + fin(i, 17) + fin(i, 18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 14);
        double Y_P1 = fin(i, 4) + fin(i, 11) + fin(i, 13) + fin(i, 17) + fin(i, 18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 16) + fin(i, 18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i, 12) + fin(i, 15) + fin(i, 17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }

    void stream (int i, int k, int iX, int iY, int iZ, double pop_out) {
        int XX = iX + c[k][0];
        int YY = iY + c[k][1];
        int ZZ = iZ + c[k][2];
        if (periodic) {
            XX = (XX + dim.nx) % dim.nx;
            YY = (YY + dim.ny) % dim.ny;
            ZZ = (ZZ + dim.nz) % dim.nz;
        }
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, opp[k]) = pop_out + f(nb, k);
        }
        else {
            fout(nb, k) = pop_out;
        }
    };

    auto collideBgk(int i, int k, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
        double eq = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        double eqopp = eq - 6.* rho * t[k] * ck_u;
        double pop_out = (1. - omega) * fin(i, k) + omega * eq;
        double pop_out_opp = (1. - omega) * fin(i, opp[k]) + omega * eqopp;
        return std::make_pair(pop_out, pop_out_opp);
    }

    void iterateBgk(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            for (int k = 0; k < 9; ++k) {
                auto[pop_out, pop_out_opp] = collideBgk(i, k, rho, u, usqr);

                stream(i, k, iX, iY, iZ, pop_out);
                stream(i, opp[k], iX, iY, iZ, pop_out_opp);
            }

            for (int k: {9}) {
                double eq = rho * t[k] * (1. - usqr);
                fout(i, k) =  (1. - omega) * fin(i, k) + omega * eq;
            }
        }
    }

    // eq_minus and eq_plus are computed via macroscopic variables
    // f_minus is computed via f_plus
    auto collideTrt(int i, int k, double rho, std::array<double, 3> const& u, double usqr) {
        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];

        double eq_plus = rho * t[k] * (1. + 4.5 * ck_u * ck_u - usqr);
        double eq_minus = rho * t[k] * 3. * ck_u;
        double pop_in = fin(i, k);
        double pop_in_opp = fin(i, opp[k]);
        double f_plus = 0.5 * (pop_in + pop_in_opp);
        double f_minus = f_plus - pop_in_opp;

        double pop_out = pop_in - s_plus * (f_plus- eq_plus) - s_minus * (f_minus - eq_minus);
        double pop_out_opp = pop_in_opp - s_plus * (f_plus - eq_plus) + s_minus * (f_minus - eq_minus);

        return std::make_pair(pop_out, pop_out_opp);
    }

    void iterateTrt(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            for (int k = 0; k < 9; ++k) {
                auto[pop_out, pop_out_opp] = collideTrt(i, k, rho, u, usqr);

                stream(i, k, iX, iY, iZ, pop_out);
                stream(i, opp[k], iX, iY, iZ, pop_out_opp);
            }

            for (int k: {9}) {
                double eq = rho * t[k] * (1. - usqr);
                fout(i, k) =  (1. - omega) * fin(i, k) + omega * eq;
            }
        }
    }

    void operator() (double& f0) {
        if (model == LBModel::bgk) {
            iterateBgk(f0);
        }
        else {
            iterateTrt(f0);
        }
    }
};

} // namespace twopop_soa
